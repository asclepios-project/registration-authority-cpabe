# Asclepios Registration Authority
In order to connect with the api/vi/auth/** APIs, the Keycloak user must have the "admin" role



## Development

To run the component locally:

```$shell
$ docker-compose up -d
```

By default the component is connected to the Keycloak server at https://asclepios-auth.euprojects.net

This, along with the Keycloak Realm and Client can be changed by the following values in the docker-compose:
 
```shell
KEYCLOAK_URL
KEYCLOAK_REALM
KEYCLOAK_CLIENT
KEYCLOAK_SECRET
```

