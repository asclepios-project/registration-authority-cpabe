package co.junwei.cpabe;

import java.io.IOException;
import java.util.Map;

public class Demo {

    final static boolean DEBUG = true;

    static String dir = "cpabe/demo/cpabe";

    static String pubfile = dir + "/pub_key";
    static String mskfile = dir + "/master_key";
    static String prvfile1 = dir + "/prv_key1";

    static String[] attr = {"baf1", "fim1", "foo"};

    static String student1_attr = "at2:yy  at3:zz";

    private Cpabe test;

    public Demo() throws IOException {
        test = new Cpabe();
    }

    public void init() throws Exception {

        println("//start to setup");
        test.setup(pubfile, mskfile);       //Once by ABE Server. One pub/master key for ALL sessions
        println("//end to setup");
    }

    public byte[] keygen(Map<String, String> attributes) throws Exception{

        String customatts;
        StringBuilder builder = new StringBuilder();
        attributes.forEach((k,v) -> builder.append(k).append(":").append(v).append(" "));
        customatts = builder.toString();

        println("//start to keygen");
        byte[] privateKey = test.keygen2(pubfile, prvfile1, mskfile, customatts); //userid        //User -> crt -> crt.ca.in_my_trusted_list ->  attrs (msk) - priv -> envlave or handed-over (ABAC protection)
        println("//end to keygen");

        return privateKey;
    }

    private static void println(Object o) {
        if (DEBUG) {
            System.out.println(o);
        }
    }
}
