package eu.ubitech.asclepios.rest.transfer;

import java.util.List;

public class UserDto {
  private String firstName;
  private String lastName;
  private String password;
  private String email;
  private String username;
  private String enabled;
  private List<String> keys;
  private List<String> values;
  private List<String> realmRoles;

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getPassword() { return password; }

  public String getEmail() {
    return email;
  }

  public String getUsername() {
    return username;
  }

  public String getEnabled() {
    return enabled;
  }

  public List<String> getKeys() {
    return keys;
  }

  public List<String> getValues() {
    return values;
  }

  public List<String> getRealmRoles() {
    return realmRoles;
  }
}
