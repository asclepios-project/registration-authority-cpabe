package eu.ubitech.asclepios.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class BaseRestController {


  @RequestMapping(value = "/", method = RequestMethod.GET)
  private ResponseEntity retrieveAll() {
    return new ResponseEntity("The Registration Authority is up and running. (to recalculate the AbeKeys of all keyclaok users go to /api/v1/auth/recalculate)", HttpStatus.OK);
  }

}
