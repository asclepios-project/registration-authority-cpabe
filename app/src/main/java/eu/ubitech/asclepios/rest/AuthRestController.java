package eu.ubitech.asclepios.rest;


import eu.ubitech.asclepios.model.KeycloakUser;
import eu.ubitech.asclepios.rest.transfer.UserDto;
import eu.ubitech.asclepios.service.IAuthService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController
@RequestMapping("/api/v1/auth")
@CrossOrigin("http://localhost:8080")
public class AuthRestController {

  private final IAuthService iAuthService;


  public AuthRestController(
      @Qualifier("authServiceImpl") IAuthService iAuthService) {
    this.iAuthService = iAuthService;
  }

  @GetMapping(value = "/delete/{username}")
  private ResponseEntity deleteUser(@PathVariable("username") String username) {
    KeycloakUser user = new KeycloakUser();
    user.setUsername(username);
    iAuthService.deleteUser(iAuthService.fetchAccessToken(),iAuthService.fetchUserID(iAuthService.fetchAccessToken(),user));
    return new ResponseEntity("User " + username + " deleted", HttpStatus.OK);
  }

  @GetMapping(value = "/getusers")
  private ResponseEntity fetchAllUsers(){
    return new ResponseEntity(iAuthService.fetchAllUsers(iAuthService.fetchAccessToken()),HttpStatus.OK);
  }

  @PostMapping(value = "/add-user")
  private ResponseEntity addUser(@RequestBody UserDto userDto){
    KeycloakUser user = new KeycloakUser();
    user.setEmail(userDto.getEmail());
    user.setUsername(userDto.getUsername());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setEnabled(userDto.getEnabled());
    Map<String, String> map = new HashMap<>();
    for (int i = 0; i < userDto.getKeys().size(); i++) {
      map.put(userDto.getKeys().get(i), userDto.getValues().get(i));
    }
    user.setAttributes(map);
    String[] array = new String[userDto.getRealmRoles().size()];
    userDto.getRealmRoles().toArray(array);
    user.setRealmRoles(array);
    iAuthService.addUser(user);
    iAuthService.resetUsersPassword(iAuthService.fetchAccessToken(), iAuthService.fetchUserID(iAuthService.fetchAccessToken(),user),userDto.getPassword());
    return new ResponseEntity("Added user with id" + iAuthService.fetchUserID(iAuthService.fetchAccessToken(),user), HttpStatus.OK);
  }

  @PostMapping(value = "/edit-user/{username}")
  private ResponseEntity editUser(@RequestBody UserDto userDto, @PathVariable("username") String username){
    KeycloakUser user = new KeycloakUser();
    user.setEmail(userDto.getEmail());
    user.setUsername(userDto.getUsername());
    user.setFirstName(userDto.getFirstName());
    user.setLastName(userDto.getLastName());
    user.setEnabled(userDto.getEnabled());
    Map<String, String> map = new HashMap<>();
    for (int i = 0; i < userDto.getKeys().size(); i++) {
      map.put(userDto.getKeys().get(i), userDto.getValues().get(i));
    }
    user.setAttributes(map);
    String[] array = new String[userDto.getRealmRoles().size()];
    userDto.getRealmRoles().toArray(array);
    user.setRealmRoles(array);
    iAuthService.editUser(user);
    iAuthService.resetUsersPassword(iAuthService.fetchAccessToken(), iAuthService.fetchUserID(iAuthService.fetchAccessToken(),user),userDto.getPassword());
    return new ResponseEntity("Edited user" + user.getUsername(), HttpStatus.OK);
  }

}
