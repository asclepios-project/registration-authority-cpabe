package eu.ubitech.asclepios.model;

public class KeycloakRole {   

  public String id;  
  public boolean clientRole;
  public boolean composite;
  public String name;
  public String containerId;
  
}
