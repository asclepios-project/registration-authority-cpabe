package eu.ubitech.asclepios.service;

import eu.ubitech.asclepios.model.KeycloakUser;
import eu.ubitech.asclepios.model.Token;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface IAuthService<U> {

  Token fetchAccessToken();

  void deleteUser(Token token, String userid);

  void requestToAddUser(Token token, U user);

  String fetchUserID(Token token, U user);

  void resetUsersPassword(Token token, String userid, String psw);

  Map<String, String> fetchAvailableRoles(Token token,String userid);

  Map<String, String> fetchClientRoles(Token token);

  Map<String, String> fetchUserRoles(Token token,String userid);

  void assignRolesToUserID(Token token,KeycloakUser user,String userid,Map<String,String> rolemap);

  U addUser(U user);

  KeycloakUser editUser(U user);

  KeycloakUser recalcAbeKey(String username);

  byte[] fetchCpabeKey(Token token, String username);

  KeycloakUser fetchUser(Token token, String username);

  List<KeycloakUser> fetchAllUsers(Token token);

  Map<String,String> fetchUserAttributes(Token token, String username);

  byte[] calcAbePvtKey(Map<String,String> attributes);



}
