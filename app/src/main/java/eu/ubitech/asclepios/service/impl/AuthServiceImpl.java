package eu.ubitech.asclepios.service.impl;

import co.junwei.cpabe.Demo;
import com.google.gson.*;
import eu.ubitech.asclepios.model.*;
import eu.ubitech.asclepios.service.IAuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;

@Component
public class AuthServiceImpl implements IAuthService<KeycloakUser> {

  @Value("${keycloak.auth-server-url}")
  private String keycloakbaseurl;

  @Value("${keycloak.realm}")
  private String keycloakrealm;

  @Value("${keycloak.resource}")
  private String keycloakclient;

  @Value("${cpabe.mode}")
  private String mode;

  @Value("${admin.username}")
  private String adminUsername;

  @Value("${admin.password}")
  private String adminPassword;

  private static final Logger logger = Logger.getLogger(AuthServiceImpl.class.getName());

  @Override
  public Token fetchAccessToken() {
    Token token = new Token();
    String tokenendpoint = keycloakbaseurl + "/realms/master/protocol/openid-connect/token";
    try {
      //---Step 1---------------------------------------
      logger.info("Fetching access token first ");
      final HttpClient client = new DefaultHttpClient();
      List<NameValuePair> params = new ArrayList<NameValuePair>();
      params.add(new BasicNameValuePair("client_id", "admin-cli"));
      params.add(new BasicNameValuePair("username", adminUsername));
      params.add(new BasicNameValuePair("password", adminPassword));
      params.add(new BasicNameValuePair("grant_type", "password"));
      HttpPost post = new HttpPost(tokenendpoint);
      post.setEntity(new UrlEncodedFormEntity(params));
      //get the response code
      HttpResponse response;
      response = client.execute(post);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("Token received successfully");
      }
      String jsonString = EntityUtils.toString(response.getEntity());
      //logger.info("response: " + jsonString);

      ObjectMapper mapper = new ObjectMapper();
      //JSON from file to Object
      token = mapper.readValue(jsonString, Token.class);
    } catch (IOException ex) {
      ex.printStackTrace();
      token = null;
    }
    return token;
  }

  @Override
  public void deleteUser(Token token, String userid) {
    try {
      logger.info("Request to delete user ");
      String userendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users/" +  userid;
      final HttpClient client = new DefaultHttpClient();
      HttpDelete delete = new HttpDelete(userendpoint);
      delete.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      //delete.setHeader("content-type", "application/json; charset=utf-8");
      //get the response code
      HttpResponse response;
      response = client.execute(delete);
      logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 201) {
        logger.info("Successful Delete ");
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public void requestToAddUser(Token token, KeycloakUser user) {
    try {
      logger.info("Request to add user ");
      String userendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users";
      final HttpClient client = new DefaultHttpClient();
      HttpPost post = new HttpPost(userendpoint);
      post.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      post.setHeader("content-type", "application/json; charset=utf-8");
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper. writeValueAsString(user);
      logger.info("UserDetails: " + json);
      post.setEntity(new StringEntity(json,"UTF-8"));
      //get the response code
      HttpResponse response;
      response = client.execute(post);
      //String jsonString = EntityUtils.toString(response.getEntity());
      //logger.info("response: " + response);
      //logger.info("response: " + jsonString);
      if (response.getStatusLine().getStatusCode() == 201) {
        logger.info("Successfull creation ");
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public KeycloakUser fetchUser(Token token, String username) {
    KeycloakUser user = new KeycloakUser();
    user.setUsername(username);
    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users?username=" + username;
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("user-id fetched sucessfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          JsonElement element = jsonret.get(0);
          JsonObject jsonbobject = element.getAsJsonObject();
          if (jsonbobject.has("firstName"))
          user.setFirstName(jsonbobject.get("firstName").getAsString());
          if (jsonbobject.has("lastName"))
            user.setLastName(jsonbobject.get("lastName").getAsString());
          if (jsonbobject.has("email"))
            user.setEmail(jsonbobject.get("email").getAsString());
          user.setEnabled(jsonbobject.get("enabled").getAsString());

          if (element.getAsJsonObject().has("attributes")){
          JsonObject jsonObject = element.getAsJsonObject().getAsJsonObject("attributes");

            //Iterate over attributes field inside JsonObject
            Iterator<String> nameItr = jsonObject.keySet().iterator();
            Map<String, String> attributes = new HashMap<String, String>();
            while(nameItr.hasNext()) {
              String name = nameItr.next();
              attributes.put(name, jsonObject.get(name).getAsString());
            }
            user.setAttributes(attributes);
          }
          Map<String, String> userRoles = fetchUserRoles(token, fetchUserID(token,user));
          //remove defaults
          userRoles.remove("offline_access");
          userRoles.remove("uma_authorization");
          String[] roles= userRoles.keySet().toArray(new String[0]);
          user.setRealmRoles(roles);
        }
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return user;
  }

  @Override
  public List<KeycloakUser> fetchAllUsers(Token token) {

    List<KeycloakUser> userList = new ArrayList<>();

    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users";
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("users fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          for (int i=0;i<jsonret.size();i++){
            KeycloakUser user = new KeycloakUser();
            JsonElement element = jsonret.get(i);
            JsonObject jsonbobject = element.getAsJsonObject();
            user.setUsername(jsonbobject.get("username").getAsString());
            if (jsonbobject.has("firstName"))
              user.setFirstName(jsonbobject.get("firstName").getAsString());
            if (jsonbobject.has("lastName"))
              user.setLastName(jsonbobject.get("lastName").getAsString());
            if (jsonbobject.has("email"))
              user.setEmail(jsonbobject.get("email").getAsString());
            user.setEnabled(jsonbobject.get("enabled").getAsString());
            if (element.getAsJsonObject().has("attributes")){
              JsonObject jsonObject = element.getAsJsonObject().getAsJsonObject("attributes");

              //Iterate over attributes field inside JsonObject
              Iterator<String> nameItr = jsonObject.keySet().iterator();
              Map<String, String> attributes = new HashMap<String, String>();
              while(nameItr.hasNext()) {
                String name = nameItr.next();
                attributes.put(name, jsonObject.get(name).getAsString());
              }
              user.setAttributes(attributes);
            }
            Map<String, String> userRoles = fetchUserRoles(token, fetchUserID(token,user));
            //remove defaults
            userRoles.remove("offline_access");
            userRoles.remove("uma_authorization");
            String[] roles= userRoles.keySet().toArray(new String[0]);
            user.setRealmRoles(roles);
            userList.add(user);
          }
        }
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return userList;
  }

  @Override
  public Map<String, String> fetchUserAttributes(Token token, String username) {
    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users?username=" + username;
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("user-id fetched sucessfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          JsonElement element = jsonret.get(0);
          return new Gson().fromJson(element.getAsJsonObject().getAsJsonObject("attributes").toString(), Map.class);
        }
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return null;
  }

  @Override
  public String fetchUserID(Token token, KeycloakUser user) {

    String userid = null;
    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users?username=" + user.username;
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("user-id fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          JsonElement element = jsonret.get(0);
          JsonObject jsonbobject = element.getAsJsonObject();
          userid = jsonbobject.get("id").getAsString();
          logger.info("user id: " + userid);
        }
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return userid;
  }

  @Override
  public byte[] fetchCpabeKey(Token token, String username) {
    byte[] decodedkey = null;
    try {
      logger.info("Request to fetch userid ");
      String usersearchendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users?username=" + username;
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(usersearchendpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("user-id fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        if (jsonret.size() > 0) {
          JsonElement element = jsonret.get(0);
          JsonObject jsonbobject = element.getAsJsonObject().getAsJsonObject("attributes");
          String cpabe = jsonbobject.get("cpabe").getAsString();
          logger.info("cpabe private key: " + cpabe);
          decodedkey = Base64.getDecoder().decode(cpabe);
        }
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return decodedkey;
  }

  @Override
  public void resetUsersPassword(Token token, String userid, String psw) {
    try {
      logger.info("Request to reset user's password ");
      String userpasswordpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users/" + userid + "/reset-password";
      final HttpClient client = new DefaultHttpClient();
      HttpPut put = new HttpPut(userpasswordpoint);
      put.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      put.setHeader("content-type", "application/json");
      KeycloakCredentials credentials = new KeycloakCredentials("password", psw, false);
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper. writeValueAsString(credentials);
      //logger.info("json: " + json);
      put.setEntity(new StringEntity(json));
      //get the response code
      HttpResponse response;
      response = client.execute(put);
      //logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 204) {
        logger.info("Password reset successfully");
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public Map<String, String> fetchAvailableRoles(Token token, String userid) {

    Map<String,String> rolemap = new HashMap<>();
    try {
      logger.info("Request to fetch roles for userid ");
      String fetchrolesforuserpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users/" + userid + "/role-mappings/realm/available";
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(fetchrolesforuserpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      //Key = rolename , Value = ID
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("roles fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        for (int i = 0;i < jsonret.size();i++) {
          JsonElement roleelement = jsonret.get(i);
          JsonObject roleobject = roleelement.getAsJsonObject();
          String roleid = roleobject.get("id").getAsString();
          //String rolename = "axip";
          String rolename = roleobject.get("name").getAsString();
          //logger.info("Role id: " + roleid + " " + rolename);
          rolemap.put(rolename, roleid);
        }
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return rolemap;
  }

  @Override
  public Map<String, String> fetchClientRoles(Token token) {
    Map<String,String> rolemap = new HashMap<>();
    try {
      logger.info("Request to fetch roles for userid ");
      String fetchrolesforuserpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/roles";
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(fetchrolesforuserpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      //Key = rolename , Value = ID
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("roles fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        for (int i = 0;i < jsonret.size();i++) {
          JsonElement roleelement = jsonret.get(i);
          JsonObject roleobject = roleelement.getAsJsonObject();
          String roleid = roleobject.get("id").getAsString();
          //String rolename = "axip";
          String rolename = roleobject.get("name").getAsString();
          //logger.info("Role id: " + roleid + " " + rolename);
          rolemap.put(rolename, roleid);
        }
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    //remove defaults
    rolemap.remove("offline_access");
    rolemap.remove("uma_authorization");
    return rolemap;
  }

  @Override
  public Map<String, String> fetchUserRoles(Token token, String userid) {
    Map<String,String> rolemap = new HashMap<>();
    try {
      logger.info("Request to fetch roles for userid ");
      String fetchrolesforuserpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users/" + userid + "/role-mappings/realm";
      final HttpClient client = new DefaultHttpClient();
      HttpGet get = new HttpGet(fetchrolesforuserpoint);
      get.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      get.setHeader("content-type", "application/json");

      //get the response code
      HttpResponse response;
      response = client.execute(get);
      //logger.info("response: " + response);
      //Key = rolename , Value = ID
      if (response.getStatusLine().getStatusCode() == 200) {
        logger.info("roles fetched successfully");
        String jsonString = EntityUtils.toString(response.getEntity());
        //logger.info("response: " + jsonString);
        JsonArray jsonret = new JsonParser().parse(jsonString).getAsJsonArray();
        for (int i = 0;i < jsonret.size();i++) {
          JsonElement roleelement = jsonret.get(i);
          JsonObject roleobject = roleelement.getAsJsonObject();
          String roleid = roleobject.get("id").getAsString();
          //String rolename = "axip";
          String rolename = roleobject.get("name").getAsString();
          //logger.info("Role id: " + roleid + " " + rolename);
          rolemap.put(rolename, roleid);
        }
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return rolemap;
  }

  @Override
  public void assignRolesToUserID(Token token, KeycloakUser user, String userid, Map<String, String> rolemap) {
    try {
      logger.info("Request to modify roles ");
      String roleendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users/" + userid + "/role-mappings/realm";
      final HttpClient client = new DefaultHttpClient();
      HttpPost post = new HttpPost(roleendpoint);
      post.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
      post.setHeader("content-type", "application/json; charset=utf-8");

      //String[] rolenames = user.clientRoles.get("backend-service");
      String[] rolenames = user.realmRoles;
      KeycloakRole[] kroles = new KeycloakRole[rolenames.length];
      for (int i = 0;i < rolenames.length;i++) {
        KeycloakRole krole = new KeycloakRole();
        krole.composite = false;
        krole.clientRole = false;
        krole.name = rolenames[i];
        krole.containerId = keycloakrealm;
        krole.id = rolemap.get(krole.name);
        kroles[i] = krole;
      }
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper. writeValueAsString(kroles);
      //logger.info("json: " + json);
      post.setEntity(new StringEntity(json,"UTF-8"));
      //get the response code
      HttpResponse response;
      response = client.execute(post);
      logger.info("response: " + response);
      if (response.getStatusLine().getStatusCode() == 204) {
        logger.info("Roles Assigned Successfully ");
      }

    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public KeycloakUser addUser(KeycloakUser user) {

    try {
      //---Step 1 Fetch Access Token -----------------
      Token token = fetchAccessToken();
      //check if user exists already
      String existinguserid = fetchUserID(token, user);
      if (existinguserid != null) {
        logger.info("User exists already: " + existinguserid);
        deleteUser(token, existinguserid);
      }

      if (user.getAttributes() == null) throw new Exception("No user attributes found");
      Map<String, String> atts = user.getAttributes();

      byte[] privateKey = calcAbePvtKey(atts);
      String cpabe = it.unisa.dia.gas.plaf.jpbc.util.io.Base64.encodeBytes(privateKey);

      //Split the String key into 4 parts
/*        int index = 0, i= 1;
      while (index < cpabe.length()) {
        atts.put("cpabe"+i, cpabe.substring(index, Math.min(index + cpabe.length()/4,cpabe.length())));
        index += cpabe.length()/4;
        i++;}*/
      atts.put("cpabe",cpabe);
      user.setAttributes(atts);

      //---Step 2 Request to add User -----------------
      requestToAddUser(token, user);
      //---Step 3 Fetch UserID-------------------------
      String userid = fetchUserID(token, user);
      //---Step 4 Reset User's Password-----------------
      resetUsersPassword(token, userid, "!asclepios!");
      //---Step 5 Fetch roles for userid ---------------
      Map<String,String> rolemap = new HashMap<>();
      rolemap = fetchAvailableRoles(token, userid);
      //---STep 6 Assign Roles -------------------------
      assignRolesToUserID(token, user, userid, rolemap);

    } catch (Exception ex) {
      ex.printStackTrace();
      return null;
    }
    return user;
  }

  @Override
  public KeycloakUser editUser(KeycloakUser user) {
    Token token = fetchAccessToken();
    logger.info("Request to edit user ");
    String userid = fetchUserID(token,user);
    String editendpoint = keycloakbaseurl + "/admin/realms/"+keycloakrealm+"/users/" + userid ;
    final HttpClient client = new DefaultHttpClient();
    HttpPut put = new HttpPut(editendpoint);
    put.setHeader(HttpHeaders.AUTHORIZATION, "bearer " + token.access_token);
    put.setHeader("content-type", "application/json; charset=utf-8");
    ObjectMapper mapper = new ObjectMapper();

    Map<String, String> atts = user.getAttributes();

    byte[] privateKey = calcAbePvtKey(atts);
    String cpabe = it.unisa.dia.gas.plaf.jpbc.util.io.Base64.encodeBytes(privateKey);
    atts.put("cpabe",cpabe);
    user.setAttributes(atts);

    try {
      String json = mapper. writeValueAsString(user);
      logger.info("UserDetails: " + json);
      put.setEntity(new StringEntity(json,"UTF-8"));
      //get the response code
      HttpResponse response;
      response = client.execute(put);
      if (response.getStatusLine().getStatusCode() == 201 || response.getStatusLine().getStatusCode() == 204) {
        logger.info("Edited user "+ user.getUsername());
      }
    }
    catch (Exception e){
      e.printStackTrace();
    }

    return user;
  }

  @Override
  public KeycloakUser recalcAbeKey(String username) {
    KeycloakUser user = fetchUser(fetchAccessToken(),username);
    addUser(user);
    return user;
  }

  @Override
  public byte[] calcAbePvtKey(Map<String, String> attributes) {
    try {
      Demo demo = new Demo();
      if (mode.equals("init"))demo.init();
      return demo.keygen(attributes);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }
}

