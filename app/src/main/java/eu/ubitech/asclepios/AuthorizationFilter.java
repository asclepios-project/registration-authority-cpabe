/*
 * Copyright (C) 2020-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.ubitech.asclepios;

import eu.asclepios.authorization.abac.util.CheckAccessResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@Component
public class AuthorizationFilter implements Filter {
    private boolean authorizationDisabled;

    @Override
    public void init(final FilterConfig filterConfig) {
        log.info("-----------------------------------------------------------------");

        // Initialize authorization service client
        authorizationDisabled = eu.asclepios.authorization.abac.client.WebAccessRequestHelper
                .getSingleton().getAuthorizationServiceClient().getProperties().getPdp().isDisabled();
        if (!authorizationDisabled)
            log.info("AZ: Authorization client ready");

        log.info("-----------------------------------------------------------------");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        if (checkAccess(req, res)) {
            filterChain.doFilter(request, response);
        }
    }

    private boolean checkAccess(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        if (authorizationDisabled) {
            log.warn(">>> AZ:    Authorization is disabled. Access granted. ");
            return true;
        }

        CheckAccessResponse response;
        boolean permit;
        try {
            log.info("AZ:    Calling PDP...");
            response = eu.asclepios.authorization.abac.client.WebAccessRequestHelper.getSingleton()
                    .getCheckWebAccessResponse(servletRequest, "resource", "action", "subject", null);
            permit = (response == null) || response.isPermit();
            log.debug("AZ:    PDP response: {}", response);
            log.info("AZ:    PDP decision: {}", permit?"PERMIT":"DENY");
        } catch (Exception e) {
            log.error("AZ:    EXCEPTION: ", e);
            permit = false;
        }

        if (! permit) {
            log.warn("AZ:    Access denied. Sorry. ");
            servletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "YOU CANNOT ACCESS THE SENSITIVE DATA");
            return false;
        } else {
            log.info("AZ:    Access granted. ");
            return true;
        }
    }
}

