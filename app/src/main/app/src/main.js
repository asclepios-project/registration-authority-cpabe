import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router';
import mdiVue from 'mdi-vue'
import * as mdijs from '@mdi/js'

Vue.use(mdiVue, {
  icons: mdijs
}) 

Vue.prototype.$http = axios
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
