import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../components/Home.vue'),
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('../components/Dashboard.vue'),
  },
  {
    path: '/add-user',
    name: 'AddUser',
    component: () => import('../components/AddUser.vue'),
  },
  {
    path: '/edit-user/:id',
    name: 'EditUser',
    component: () => import('../components/EditUser.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
