FROM openjdk:8-jdk-slim

COPY ./app/target/app-1.0-SNAPSHOT.jar /registration_auth/
COPY ./cpabe/cpabe-api/target/cpabe-api-1.0-SNAPSHOT.jar /cpabe-api/
COPY ./cpabe/cpabe-demo/target/cpabe-demo-1.0-SNAPSHOT.jar /cpabe-demo/
COPY ./truststore-client.p12 /registration_auth/

COPY ./cpabe/demo/cpabe /cpabe/demo/cpabe

ENV AZ_CLIENT_TRUST_STORE_FILE=/registration_auth/truststore-client.p12
ENV AZ_CLIENT_TRUST_STORE_TYPE=PKCS12
ENV AZ_CLIENT_TRUST_STORE_PASSWORD=asclepios
ENV AZ_SERVER_ACCESS_KEY=7235687126587231675321756752657236156321765723
ENV AZ_CALL_DISABLED=false
ENV AZ_CALL_LOAD_BALANCE_METHOD=ORDER

CMD ["java", "-jar", "/registration_auth/app-1.0-SNAPSHOT.jar", "--spring.profiles.active=production"]
